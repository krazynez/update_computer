#!/bin/bash




update_program(){
clear


movePath=$(basename "$PWD");

if [[ $movePath == "update_computer-master" ]]; then
    cd ..
    path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P );
    mv update_computer-master $path/update_computer
    cd update_computer
    main
    exit 0;

fi

path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P );

##### AUTO UPDATER ###########


echo ""
echo ""
echo "Checking for update... Please wait..."

if [[ ! -f "/usr/bin/git" ]]; then
    clear
    echo ""
    echo "You need to install git first..."
    read -p "Would you like to install git? y/n: " gitInstall

    if [[ "$gitInstall" == "y" || "$gitInstall" == "Y" ]]; then
        sudo apt install git -y
        main
        exit 0;


    else
        exit 0;

    fi
fi


####### Checks if head is the same from github ##############


check="$(git fetch -v --dry-run 2>&1)"
echo "${check}" > .update.txt
	if [ -f ".old_update.txt" ]; then
        check1=$(<.old_update.txt) 2>&1

    else
        touch .old_update.txt
	fi

	if [[ "${check}" == "${check1}" ]]; then
        rm .update.txt
        clear
	fi

	if [[ "${check}" != "${check1}" ]]; then

        git fetch https://github.com/krazynez/update_computer.git
        git clone https://github.com/krazynez/update_computer.git temp
        rsync -a temp/ $path
        echo "${check}" > .old_update.txt
        rm -rf temp

        main

        exit 0;

	fi


 	if [[ "$?" == "1" ]]; then
        clear
        echo ""
        echo "something went wrong"

        sleep 3

	fi

############# Checking for useless files ##############

path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

cd $path



if [[ -f ".update.sh" ]]; then
 rm -rf ".update.sh"

    elif [[ -f ".update.txt" ]]; then
    rm -rf .update.txt

        elif [[ -f ".update1.sh" ]]; then
        rm -rf .update1.sh

               elif [[ -f "test.sh use for debugging" ]]; then
               rm -rf "test.sh use for debugging"

                       elif [[ -f "_update1.sh - do not use" ]]; then
                       rm -rf "_update1.sh - do not use"

fi

####### Make Welcome.sh Executable #####

clear
echo "#######################################################"
echo "#                                                     #"
echo "# Checking if Welcome.sh is executable so you can run #"
echo "# as ./Welcome.sh instead of bash Welcome.sh          #"
echo "#                                                     #"
echo "#######################################################"
echo ""
if [[ ! -x "Welcome.sh" ]]; then
    sudo chmod +x Welcome.sh
    main

else
    clear
    echo ""
fi






}








####################### Check to see if screenfetch is installed
screenFetch(){

	if [ -f /usr/bin/neofetch ]; then

            clear

            neofetch

            read -p "Press enter to continue"

            main

            exit 0;





	else
                    clear
                    echo ""
                    echo ""

                    echo "You do not have screenfetch installed"
                    echo ""

                    read -p "Do you want to install screenfetch? y/n: " ask




                              if [ "$ask" == "y" ]; then

                                                sudo apt install neofetch -y && neoFetch;


                                                else


                                                main

                                                exit 0;


                            fi

    fi




while (( "$ask" != "y" )) || (( "$ask" != "n" )); do

    clear

    echo ""

    echo "You need to say 'y/n'"

    sleep 2

    done







}





update_computer(){


clear


path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

cd $path



echo ""
echo ""


read -p "Do you want to update your computer? y/n: " upd

	until [ "$upd" == "y" ] || [ "$upd" == "n" ]; do
		clear
		echo ""
		echo ""
		echo "Sorry you need to enter y/n. Please try again."
clear
echo ""
echo ""

read -p "Do you want to update your computer? y/n: " upd




done

########## completely updates and removes old packages ###########

if [ "$upd" == "y" ];
then
sudo apt update && sudo apt upgrade -yy && sudo apt dist-upgrade -yy && sudo apt autoremove -yy && sudo apt autoclean

else
	clear
	exit 0;

fi








clear
echo ""
echo ""

	read -p "Do you want to see if you can uninstall old kernels? y/n: " ask

	if [ "$ask" == "y" ]; then
		kernelUninstall
	else
		clear
		exit 0;
    fi
}







######### Uninstall old kernels #################
kernelUninstall(){


			clear
            printf '\e[8;15;200t'
			kern=$(uname -r)
	        YELLOW='\033[1;33m'
	        NC='\033[0m'
			echo "##########################################"
			echo -e "# Current Kernel: ${YELLOW}$kern${NC}       #"
			echo "# IF LASTEST DO NOT REMOVE THIS kERNEL   #"
			echo "##########################################"

			echo ""
			echo ""
			dpkg --list | grep linux-image
			echo ""
			echo ""
			echo "###########################################################################"

			read -p "Copy and Paste what kernel you want to delete i.e linux-image-X.X.X-XX-generic or exit to exit: " k



				if [ "$k" == "exit" ]; then
						printf '\e[8;24;80t'
						clear
						exit 0;
				elif [ "$k" == "" ]; then
						update_computer
				elif [ "$k" == " " ]; then
						update_computer
				else
					echo ""

				fi

				if [ -f "/usr/bin/lxterminal" ]; then
					if [ -d "/usr/bin/xpad-0.4" ]; then
						sudo apt purge $dk && cd /usr/src/xpad-0.4 && sudo git fetch && sudo git checkout origin/master && sudo dkms remove -m xpad -v 0.4 --all && sudo dkms install -m xpad -v 0.4 && cd $path && sudo update-grub
						lxterminal --geometry=80x24 -e bash Welcome.sh

					else
						sudo apt purge $dk && sudo update-grub
						lxterminal --geometry=80x24 -e bash Welcome.sh
					fi
				fi

				if [ -f "/usr/bin/gnome-terminal" ]; then


					if [ -d "/usr/src/xpad-0.4" ]; then
						sudo apt purge $k && cd /usr/src/xpad-0.4 && sudo git fetch && sudo git checkout origin/master && sudo dkms remove -m xpad -v 0.4 --all && sudo dkms install -m xpad -v 0.4 && cd $path && sudo update-grub
						printf '\e[8;24;80t'
					else
						sudo apt purge $k && sudo update-grub
						printf '\e[8;24;80t'
					fi
				fi



}





dpkg_fix(){
    sudo apt install -f

    main

    exit 0;

}




close(){
    clear
    exit 0;



}




main(){
###### ACTUAL PROGRAM #####################
if [[ -f "/etc/arch-release" ]]; then
  bash arch.sh
  exit 0;
fi

update_program


####### Check if parent or child process is already running ##########

if pidof -o %PPID -x "Welcome.sh" >/dev/null; then
    echo "Welcome.sh already running"
    sleep 5
    exit 0;
fi





clear
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%          Update_Computer v1.0.4           %"
echo "%                                           %"
echo "%                                           %"
echo "%                                           %"
echo "% 1.) About computer   2.) Update computer  %"
echo "%                                           %"
echo "% 3.) Fix dpkg 'lock'  4.) Exit             %"
echo "%                                           %"
echo "%                                           %"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo ""

# Ask what option to use:

read -p "Which option do you wish to choose?: " option


# Start Error trap



while (( "$option" < "1" )) || (( "$option" > "4" )); do
    clear
    echo ""
    echo ""

    echo "Please choose options 1-4"


    clear
    echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    echo "%          Update_Computer v1.0.4           %"
    echo "%                                           %"
    echo "%                                           %"
    echo "%                                           %"
    echo "% 1.) About computer   2.) Update computer  %"
    echo "%                                           %"
    echo "% 3.) Fix dpkg 'lock'  4.) exit             %"
    echo "%                                           %"
    echo "%                                           %"
    echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    echo ""

    read -p "Which option do you wish to choose?: " option


if (( "$option" >= "1" )) && (( "$option" <= "4" )); then


        break

        else

        continue

fi

        done


        # End Error trap


if [ "$option" == "" ]; then
		clear
		echo ""
		echo ""
		echo "You need to enter a number try again..."
		sleep 2
		main
		exit 0;


fi

case $option in
    1) neoFetch;;
    2) update_computer;;
    3) dpkg_fix;;
    4) close;;
    255) echo "Something went wrong.. try again.." && sleep 3; break;;
esac
}
main
