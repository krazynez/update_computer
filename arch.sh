#!/bin/bash



################ ABOUT COMPUTER SECTION #####################

neoFetch(){

if [[ -f "/usr/bin/neofetch" ]]; then
        clear
        neofetch
        read -p "Press enter to continue"
        main
        exit

    else
        clear
        echo "You do not have neofetch installed"
        read -p "Do you want to install neofetch? y/n: " response


            case $response in
                'y'|'Y') sudo pacman -S neofetch  && clear && neofetch && read -p "Press enter to continue" && main;;
                'n'|'N') main;;
                255) echo "Something went wrong"; break;;
            esac
    fi

}


####################### Updates Your Computer ###################



Update_computer(){

#path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

#cd $path

clear
echo ""
########## completely updates and removes old packages ###########
read -p "Do you want to update your computer? y/n: " update


case "$update" in

    "y"|"Y") sudo pacman -Syu && aurman -Syu;;
    "n"|"N") clear && exit 0;;
    255) echo "something went wrong";;
esac

kernelUninstall

}


######### Uninstall old kernels #################
kernelUninstall(){
  clear
  echo "Kernel Uninstaller: "
  echo "Not Supported Yet..."
  sleep 5
  main
}
# kernelUninstall(){
# 	dialog --title "Kernel Uninstall" --defaultno --yesno "Do you want to see if you can uninstall old kernels?" 10 30
#     kernel=$?
#
#
#
#
#
#
# 		case $kernel in
#        0) kern=$(uname -r)
# 		dialog --infobox "!DO NOT DELETE THIS IF MOST CURRENT! \nCurrent Kernel: $kern" 10 50
#         sleep 10
#        kernel=$(dpkg --list | grep linux-image)
# 		 dialog --title "Copy just linux-image-X.X.X-XX-generic" --backtitle "Kernel to uninstall" --infobox "$kernel" 15 70
#         sleep 20;;
#
#
#         1) clear && exit 0;;
#            esac
# local deleteKern=$(tempfile 2>/dev/null)
#
# dialog --title "CTRL + SHIFT + V for the kernel you want to delete i.e linux-image-X.X.X-XX-generic" --backtitle "Select Kernel to Uninstall" --defaultno --inputbox "Kernel: " 15 70 2>$deleteKern
# kernel=$?
# dk=$(<"${deleteKern}")
# clear
#         case $kernel in
# 		0) if [ -d "/usr/src/xpad-0.4" ]; then
# 					sudo apt purge $dk && cd /usr/src/xpad-0.4 && sudo git fetch && sudo git checkout origin/master && sudo dkms remove -m xpad -v 0.4 --all && sudo dkms install -m xpad -v 0.4 && cd $path && sudo update-grub
# 					printf '\e[8;24;80t'
#             if [ -f "/usr/bin/lsterminal" ]; then
#                 if [ -d "/usr/src/xpad-0.4" ]; then
# 					sudo apt purge $dk && cd /usr/src/xpad-0.4 && sudo git fetch && sudo git checkout origin/master && sudo dkms remove -m xpad -v 0.4 --all && sudo dkms install -m xpad -v 0.4 && cd $path && sudo update-grub
# 					lxterminal --geometry=80x24 -e main
#
#                 else
# 					sudo apt purge $dk && sudo update-grub
# 					lxterminal --geometry=80x24 -e main
#
#                 fi
#             fi
#
#
#
# 		else
# 					sudo apt purge $dk && sudo update-grub
# 					printf '\e[8;24;80t'
# 		fi;;
#         1) exit 0;;
#         255) echo "something went wrong"; break;;
#         esac
# }

######## Exit ##########


Exit(){

    clear
    exit 0;
}



main(){
###### ACTUAL PROGRAM #####################

####### Check if parent or child process is already running ##########

if pidof -o %PPID -x "arch.sh" >/dev/null; then
    echo "arch.sh already running"
    sleep 5
    exit 0;
fi




clear
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%          Update_Computer v1.0.5           %"
echo "%                                           %"
echo "%                                           %"
echo "%                                           %"
echo "% 1.) About computer   2.) Update computer  %"
echo "%                                           %"
echo "% 3.) Exit                                  %"
echo "%                                           %"
echo "%                                           %"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo ""

# Ask what option to use:

read -p "Which option do you wish to choose?: " option


# Start Error trap



while (( "$option" < "1" )) || (( "$option" > "3" )); do
    clear
    echo ""
    echo ""

    echo "Please choose options 1-3"


    clear
    echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    echo "%          Update_Computer v1.0.5           %"
    echo "%                                           %"
    echo "%                                           %"
    echo "%                                           %"
    echo "% 1.) About computer   2.) Update computer  %"
    echo "%                                           %"
    echo "% 3.) Exit                                  %"
    echo "%                                           %"
    echo "%                                           %"
    echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    echo ""

    read -p "Which option do you wish to choose?: " option


if (( "$option" >= "1" )) && (( "$option" <= "3" )); then


        break

        else

        continue

fi

        done


        # End Error trap


if [ "$option" == "" ]; then
		clear
		echo ""
		echo ""
		echo "You need to enter a number try again..."
		sleep 2
		main
		exit 0;


fi

case $option in
    1) neoFetch;;
    2) Update_computer;;
    3) Exit;;
    255) echo "Something went wrong.. try again.." && sleep 3; break;;
esac
}
main
